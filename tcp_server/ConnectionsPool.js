class ConnectionsPool {
    
    constructor() {
        const DeviceData = require('./DeviceData');
        const DecoderData = require('./DecoderData');
        const phpFunction = require('./phpfunction');
        this.DEVICE = new DeviceData();
        this.DECODER = new DecoderData();
        this.PHP = new phpFunction();
        this.CONNECTIONS = {};
          }

   
    add(connection) {

        this.initEvents(connection);
    }

    initEvents(connection) {
        connection.on('data', data => {
          



            var isValidJSON = true;
            try {
                var json = JSON.parse(data);
            } catch (e) {
                isValidJSON = false;
            }

            if (isValidJSON) {
                if (json.pwd === "zafer") {
                    this.sendTO(json.id, json.cmd);
                } else {
                    connection.write('PASSWORD_NOT_GOOD');
                }
                return;
            }

            let typePacket = this.DEVICE.identificationPacket(data);


            if (typePacket == 'POST') {
               var postwrite= connection.write("HTTP/1.1 100 Continue\r\n");
                return;
            }

            if (typePacket == 'ERROR') {
                console.log('GET ERROR DEVICE ' + connection.ID);
                console.log('DATA ' + data);
                return;
            }

            if (typePacket == 'RESPONSE') {
                var CMD = this.DEVICE.sendCommand("#ACK#" + this.PHP.hex2bin('2000'));
                var responsewrite = connection.write(CMD);
                console.log('GET RESPONSE DEVICE ' + connection.ID + 'data' + data + 'write  ' + responsewrite);
                return;
            }



            if (typePacket == 'DATA') {
                if (typeof this.CONNECTIONS[connection.ID] === 'undefined') {
                    var device_id = this.DEVICE.getID(data);
                    connection.ID = device_id;
                    this.CONNECTIONS[connection.ID] = connection;
                    console.log('CONNECT NEW DEVICE' + connection.ID);
                }

                var DEVICE = this.DECODER.Start(data);
                 connection.write(DEVICE['confirmation']);
                this.DEVICE.Save_to_db(DEVICE);

            }            
        });

        connection.on('timeout', data => {
            console.log('SOCKET TIMEOUT id: ' + connection.ID + ' DATA: ' + data);
        });

        connection.on('close', data => {
            console.log('dissconect device id: ' + connection.ID + ' DATA: ' + data);

        });
        connection.on('error', data => {
            console.log('ERROR device id: ' + connection.ID + ' DATA: ' + data);
        });
    }

    getConnectionData(connection) {
        var result = this.CONNECTIONS[connection.ID];

        return result;
    }

    findConnectionData(id) {

        var result = this.CONNECTIONS[id];

        return result;
    }

    sendTO(id, cmd) {
        console.log('id ' + id);
        console.log('cmd ' + cmd);
        var client = this.findConnectionData(id);

        if (typeof client === 'undefined') {
            console.log('NOTFOUND client');
        } else {
            client.write(cmd);
           
        }
       

    }
}
module.exports = ConnectionsPool;

