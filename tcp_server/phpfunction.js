﻿class phpfunction
{

    hex2bin(s) {
        var ret = []
        var i = 0
        var l

        s += ''

        for (l = s.length; i < l; i += 2) {
            var c = parseInt(s.substr(i, 1), 16)
            var k = parseInt(s.substr(i + 1, 1), 16)
            if (isNaN(c) || isNaN(k)) return false
            ret.push((c << 4) | k)
        }

        return String.fromCharCode.apply(String, ret)
    }

    bin2hex(bin) {
        var hex = '';
        
        for (var i = 0; i < bin.length; i++) {
            var c = bin.charCodeAt(i);

            hex += c.toString(16);
        }
        
        return hex
}
    

    chunk_split(body, chunklen, end) {
        chunklen = parseInt(chunklen, 10) || 76
        end = end || '\r\n'

        if (chunklen < 1) {
            return false
        }

        return body.match(new RegExp('.{0,' + chunklen + '}', 'g'))
            .join(end)
        }

    str_split(f_string, f_split_length, f_backwards) {	// Convert a string to an array
    // 
    // +	 original by: Martijn Wieringa

    if (f_backwards == undefined) {
        f_backwards = false;
    }

    if (f_split_length > 0) {
        var result = new Array();

        if (f_backwards) {
            var r = (f_string.length % f_split_length);

            if (r > 0) {
                result[result.length] = f_string.substring(0, r);
                f_string = f_string.substring(r);
            }
        }

        while (f_string.length > f_split_length) {
            result[result.length] = f_string.substring(0, f_split_length);
            f_string = f_string.substring(f_split_length);
        }

        result[result.length] = f_string;
        return result;
    }

    return false;
}


}

module.exports = phpfunction;