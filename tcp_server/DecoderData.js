﻿class DecoderData
{
    constructor() {
        const phpFunction = require('./phpfunction');
        const DeviceData = require('./DeviceData');
        this.DEVICE = new DeviceData();
        this.PHP = new phpFunction();
        
        this.device = [];
        this.device['header'] = [];

        this.Sdevice = [];
        this.Sdevice['data'] = [];
        this.Sdevice['data']['data'] = [];

    }

    Start(data) {
        this.Data_Split(data);

        this.prettyHex();
        this.getHeader();
        this.device['mFD'] = this.device['binbuffer'][5];
        this.getEvents();
        this.getData();

        this.Sdevice['confirmation'] = this.DEVICE.sendCommand('#ACK#' + this.device['binbuffer'].slice(0, 1) + this.device['binbuffer'].slice(2, 3) + "\0");
        this.Sdevice['badconfirmation'] = this.DEVICE.sendCommand('#ACK#' + this.device['binbuffer'].slice(0, 1).toString('binary') + this.device['binbuffer'].slice(2, 3).toString('binary') + "\0".toString('binary'));
        const fs = require("fs");

        fs.appendFileSync(this.device['id'], this.device['bin'].split('').map(l => l.charCodeAt(0)).map(l => l.toString(16)).join('') + "\r\n" + this.Sdevice['data']['data']['timegps']+ "\r\n" + this.Sdevice['badconfirmation'] + "\r\n" + "\r\n" + "\r\n");
        return this.Sdevice;
    }

    Data_Split(data) {
       
        var exdata = data.toString('binary').split("&bin=");
        var exid = exdata[0].split("id=");

        var id = exid[1];
        this.device['id'] = id.toString();
        this.Sdevice['id'] = this.device['id'];
        this.device['bin'] = exdata[1].toString('binary');
        this.device['binbuffer'] = Buffer.from(exdata[1].toString('binary'), 'binary');
  }

 

    prettyHex() {

        this.device['hex'] = this.device['binbuffer'].toString('hex');
    }
    

    getHeader() {
        this.device['BufHeader'] = this.device['binbuffer'].slice(0, 5);

        this.device['header']['ID_PAC'] = this.device['BufHeader'][0];
        this.device['header']['OFFS_DATA'] = this.device['BufHeader'][1];
        this.device['header']['PAC_CNT'] = this.device['BufHeader'][2];
        this.device['header']['rezerv'] = this.device['BufHeader'][3];
        this.device['header']['PAR'] = this.device['BufHeader'][4];
    }

    getEvents() {
        this.device['BufEvents'] = this.device['binbuffer'].slice(6, 15);
     }

    getData() {
        try {
            this.device['BufData'] = this.device['binbuffer'].slice(15);
            this.latitude();//15-19
            this.longitude();//19-23
            this.timeGPS();//23-27
            this.Sdevice['data']['data']['week'] = this.device['binbuffer'].slice(27, 29).readInt16BE();
            this.Sdevice['data']['data']['status'] = this.device['binbuffer'].slice(29, 30).readInt8();
            this.Sdevice['data']['data']['height'] = this.device['binbuffer'].slice(30, 32).readUInt16BE();
            this.Sdevice['data']['data']['speed'] = this.device['binbuffer'].slice(32, 33).readInt8();
            this.Sdevice['data']['data']['angle_direction'] = this.device['binbuffer'].slice(33, 34).readUInt8();
            this.Sdevice['data']['data']['mileage'] = this.device['binbuffer'].slice(34, 38).readFloatBE();
            this.Sdevice['data']['data']['fuel_consumption'] = this.device['binbuffer'].slice(38, 42).readFloatBE();
            this.Sdevice['data']['data']['moto_hours'] = this.device['binbuffer'].slice(42, 46).readFloatBE();
            this.Sdevice['data']['data']['level_fuel'] = this.device['binbuffer'].slice(46, 47).readInt8();//не приходит
            this.Sdevice['data']['data']['speed_engine'] = this.device['binbuffer'].slice(47, 49).readInt16BE();
            this.Sdevice['data']['data']['pedal_position'] = this.device['binbuffer'].slice(49, 50).readInt8();
            this.Sdevice['data']['data']['engine_temperature'] = this.device['binbuffer'].slice(50, 51).readInt8();
            this.Sdevice['data']['data']['service_mileage'] = this.device['binbuffer'].slice(51, 53).readInt16BE();
            this.Sdevice['data']['data']['level_fuel_acp1'] = this.device['binbuffer'].slice(53, 55).readUInt16BE();
            this.Sdevice['data']['data']['level_fuel_acp2'] = this.device['binbuffer'].slice(55, 57).readUInt16BE();
            this.Sdevice['data']['data']['level_fuel_acp3'] = this.device['binbuffer'].slice(57, 59).readUInt16BE();
            this.Sdevice['data']['data']['level_fuel_acp4'] = this.device['binbuffer'].slice(59, 61).readUInt16BE();
            this.Sdevice['data']['data']['charge_battery_terminal'] = this.device['binbuffer'].slice(61, 62).readUInt8();
            this.Sdevice['data']['data']['sensor1'] = this.device['binbuffer'].slice(62, 63).readInt8();
            this.Sdevice['data']['data']['sensor2'] = this.device['binbuffer'].slice(63, 64).readInt8();
            this.Sdevice['data']['data']['old_sensor1'] = this.device['binbuffer'].slice(64, 65).readInt8();
            this.Sdevice['data']['data']['old_sensor2'] = this.device['binbuffer'].slice(65, 66).readInt8();
            this.Sdevice['data']['data']['voltage_battery'] = this.device['binbuffer'].slice(66, 67).readInt8();
            this.Sdevice['data']['data']['voltage_onboart'] = this.device['binbuffer'].slice(67, 68).readInt8();
            this.Sdevice['data']['data']['chip_temperature'] = this.device['binbuffer'].slice(68, 69).readUInt8();
            this.Sdevice['data']['data']['gsm_level'] = this.device['binbuffer'].slice(69, 70).readUInt8();

            this.doors();//102-103



   
        } catch (e) {
            console.log('error get data for: ' + this.device['id'] + ' bin:' + this.device['bin']);
            console.log(e);
        }
    }






    latitude() {
        var latitude;
        latitude =  this.device['binbuffer'].slice(15, 19).toString('hex');
        latitude = parseInt(latitude, 16);
        latitude = (latitude << 2);
        latitude = latitude.toString(16);
        var buf = new Buffer(latitude, 'hex');
        try {
        latitude = buf.readFloatBE();

    } catch(e) {
            latitude = 0;
    }
        latitude = latitude * 180;Math.fround
        latitude=latitude / Math.PI
        this.Sdevice['data']['data']['latitude'] = Math.fround(latitude);
    }

    longitude() {
        var longitude;
        longitude =  this.device['binbuffer'].slice(19, 23).toString('hex');
        longitude = parseInt(longitude, 16);
        longitude = (longitude << 2) ;
        longitude = longitude.toString(16);
        var buf2 = new Buffer(longitude, 'hex');
    
        try {
            longitude = buf2.readFloatBE();
        } catch (e) {
            longitude = 0;
        }
        longitude = longitude * 180;
        longitude=longitude / Math.PI
        this.Sdevice['data']['data']['longitude'] = Math.fround(longitude);
    }

    timeGPS() {
        var timeGPS;
        timeGPS = this.device['binbuffer'].slice(23, 27).toString('hex');
        timeGPS = parseInt(timeGPS, 16);
        timeGPS = (timeGPS << 2);
        timeGPS = timeGPS.toString(16);
        var buf3 = new Buffer(timeGPS, 'hex');
      
        try {
            timeGPS = buf3.readFloatBE();
        } catch (e) {
            timeGPS = 0;
        }
 
        this.Sdevice['data']['data']['timegps'] = Math.fround(timeGPS);
        this.Sdevice['data']['data']['timegetpacket'] = new Date();
    }

 
    doors() {
        var doors;
        doors = this.device['binbuffer'].slice(102, 103);
        
        doors = parseInt(doors, 16);
        doors = (doors & 4);
        if (doors == 0) {
            doors = 'OPEN';
        } else {
            doors = 'CLOSE';
        }
        this.Sdevice['data']['data']['doors'] = doors;
    }

}
module.exports = DecoderData;