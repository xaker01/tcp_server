"use strict";
const NET = require('net');
const SERVER = NET.createServer();
const PORT = 9010;
const ConnectionsPool = require('./ConnectionsPool');
const POOL = new ConnectionsPool();


SERVER.on('connection', function (connection) {

    connection.setDefaultEncoding('binary');
    connection.setEncoding('binary');
    POOL.add(connection);
    //console.log('end');
});

//запуск сервера
SERVER.listen(PORT, function () {
    console.log('server is listening');
});